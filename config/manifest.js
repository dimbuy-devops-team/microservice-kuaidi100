const path = require('path')
const plugins = [
  // request query, payload, and params sanitization
  {
    plugin: require('disinfect'),
    options: {
      deleteEmpty: true,
      deleteWhitespace: true,
      disinfectQuery: true,
      disinfectParams: true,
      disinfectPayload: true
    }
  },
  {
    plugin: require('@hapi/good'),
    options: {
      reporters: {
        console: [{
          module: '@hapi/good-squeeze',
          name: 'Squeeze',
          args: [{
            log: '*',
            response: '*'
          }]
        },
        {
          module: '@hapi/good-console'
        }, 'stdout']
      }
    }
  },
  // decorate global kuaidi100 data
  {
    plugin: './plugins/kuaidi'
  },
  // Boom
  {
    plugin: './plugins/boom'
  },
  // application routes
  {
    plugin: './plugins/route',
    routes: {
      prefix: '/api'
    },
    options: {
      dir: path.join(__dirname, '../api/routes/**/*')
    }
  },
  // health check
  {
    plugin: require('hapi-alive'),
    options: {
      method: 'GET',
      path: '/health',
      tags: ['health', 'monitor'],
      responses: {
        healthy: {
          message: '快递100 API is up and running now.',
          statusCode: 200
        },
        unhealthy: {
          message: '快递100 API is unavailable.',
          statusCode: 400
        }
      }
    }
  }
]

const manifest = {
  server: {
    router: {
      stripTrailingSlash: true,
      isCaseSensitive: false
    },
    routes: {
      security: {
        hsts: true,
        xss: true,
        noOpen: true,
        noSniff: true,
        xframe: true
      },
      cors: true,
      jsonp: 'callback',
      auth: false
    },
    host: process.env.APP_HOST,
    port: process.env.APP_PORT
  },
  register: {
    plugins
  }
}

module.exports = manifest
