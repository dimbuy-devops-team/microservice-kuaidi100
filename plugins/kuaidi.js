'use strict'

const fs = require('fs')

const internals = {
  readCompanyData: () => {
    return new Promise(function (resolve, reject) {
      fs.readFile('./kuaidi_company.json', function (err, data) {
        if (err) {
          reject(err)
        }
        resolve(JSON.parse(data))
      })
    })
  }
}

module.exports = {
  register: async (server, options) => {
    const kuaidiSource = await internals.readCompanyData()
    server.decorate('toolkit', 'kuaidiData', kuaidiSource)
  },
  name: 'hapiKuaidi',
  version: require('../package.json').version
}
