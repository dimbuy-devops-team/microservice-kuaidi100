'use strict'

const Boom = require('@hapi/boom')

const boomFunctions = [
  'badRequest',
  'unauthorized',
  'paymentRequired',
  'forbidden',
  'notFound',
  'methodNotAllowed',
  'notAcceptable',
  'proxyAuthRequired',
  'clientTimeout',
  'conflict',
  'resourceGone',
  'lengthRequired',
  'preconditionFailed',
  'entityTooLarge',
  'uriTooLong',
  'unsupportedMediaType',
  'rangeNotSatisfiable',
  'expectationFailed',
  'badData',
  'preconditionRequired',
  'tooManyRequests',
  'badImplementation',
  'internal',
  'notImplemented',
  'badGateway',
  'serverUnavailable',
  'gatewayTimeout',
  'illegal',
  'teapot',
  'boomify',
  'failedDependency'
]

exports.plugin = {
  async register (server, options) {
    boomFunctions.forEach(boomFunction => {
      server.decorate('toolkit', boomFunction, function () {
        throw Boom[boomFunction].apply(Boom, arguments)
      })
    })
  },
  name: 'hapi-plugin-boom',
  version: require('../package.json').version
}
