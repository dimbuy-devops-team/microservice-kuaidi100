'use strict'

const boom = require('@hapi/boom')

exports.index = {
  // response: {
  //   emptyStatusCode: 204
  // },
  handler: async (request, h) => {
    try {
      return h.response().code(204)
    } catch (e) {
      throw boom.badRequest(`系統錯誤: ${e.message}`)
    }
  }
}
