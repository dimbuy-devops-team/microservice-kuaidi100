'use strict'

const fetch = require('../../libs/fetch')
const waybillSchemas = require('../schemas/waybills')

exports.query = {
  validate: {
    params: waybillSchemas.queryParams
  },
  handler: async (request, h) => {
    try {
      const waybillNo = request.params.waybill_no.replace(/\s+/g, '').trim()
      const companyRaw = await fetch.call('GET', 'autonumber/autoComNum', null, { resultv2: 1, text: waybillNo })
      const companyData = h.kuaidiData

      const companys = []
      for (const company of companyData.company) {
        for (const suggest of companyRaw.auto) {
          if (suggest.comCode === company.number) {
            companys.push({
              comCode: company.number,
              companyName: company.name,
              comUrl: company.siteUrl,
              noCount: suggest.noCount
            })
          }
        }
      }
      companys.sort((a, b) => b.noCount - a.noCount)

      return h.response(companys).code(200)
    } catch (e) {
      return h.badRequest(`系統錯誤: ${e.message}`)
    }
  }
}

exports.route = {
  validate: {
    params: waybillSchemas.routeParams
  },
  handler: async (request, h) => {
    try {
      const waybillNo = request.params.waybill_no.replace(/\s+/g, '').trim()
      const companyNumber = request.params.company_number.replace(/\s+/g, '').trim()
      const routeData = await fetch.call('GET', 'query', null, { type: companyNumber, postid: waybillNo })

      if (parseInt(routeData.status) === 200) {
        const mapEmpty = routeData.data.map(function (item, index, array) {
          return `${item.time} ${item.context}`
        })

        return h.response({
          comCode: routeData.com,
          isCheck: routeData.ischeck,
          routes: mapEmpty
        }).code(200)
      } else {
        return h.badRequest('找不到資料，可能運送公司與單號不相符！')
      }
    } catch (e) {
      return h.badRequest(`系統錯誤: ${e.message}`)
    }
  }
}
