'use strict'

const Joi = require('@hapi/joi')

module.exports = {
  queryParams: Joi.object().keys({
    waybill_no: Joi.string().alphanum().required().error(new Error('Sorry... something wrong!'))
  }),
  routeParams: Joi.object().keys({
    waybill_no: Joi.string().alphanum().required().error(new Error('Sorry... something wrong!')),
    company_number: Joi.string().alphanum().required().error(new Error('Sorry... something wrong!'))
  })
}
