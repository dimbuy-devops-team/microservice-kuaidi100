'use strict'

const Waybills = require('../handlers/waybills')
const Favicons = require('../handlers/favicons')

const routes = [
  { method: 'GET', options: Favicons.index, path: '/favicon.ico' },
  { method: 'GET', options: Waybills.query, path: '/waybills/{waybill_no}' },
  { method: 'GET', options: Waybills.route, path: '/waybills/{waybill_no}/companys/{company_number}' }
]

exports.routes = server => server.route(routes)
