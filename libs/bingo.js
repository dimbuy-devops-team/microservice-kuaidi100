'use strict'

/*
 * HTTP-friendly Success 2xx objects
 * https://httpstatuses.com
 */

const internals = {}

module.exports = {
  ok: (message) => {
    return internals.create(200, message)
  },
  created: (message) => {
    return internals.create(201, message)
  },
  accepted: (message) => {
    return internals.create(202, message)
  }
}

internals.create = function (statusCode, message) {
  let output = {
    statusCode: statusCode,
    message: message
  }

  return output
}
