'use strict'

const qs = require('qs')
const got = require('got')
const FormData = require('form-data')

const client = got.extend({
  prefixUrl: process.env.API_ENPOTIN_KUAIDI100,
  headers: {
    // (tricks) specify user-agent for bypassing some site request restict detection
    'user-agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:15.0) Gecko/20100101 Firefox/15.0.1',
    'kuaidi100-application-id': 'microservice.api.kuaidi100',
    'kuaidi100-application-version': '4.0.0',
    'kuaidi100-application-type': 'api'
  }
})

exports.call = async (method, path, key, params) => {
  try {
    const options = {}
    options.method = method
    if (params) {
      if (method === 'GET') {
        options.searchParams = qs.stringify(params)
      } else {
        if (params instanceof FormData) {
          options.body = params
        } else {
          options.json = params
        }
      }
    }

    const result = await client(path, options)
    const body = JSON.parse(result.body)

    if (key) {
      return body[key]
    }

    return body || {}
  } catch (error) {
    return Promise.reject(error)
  }
}
