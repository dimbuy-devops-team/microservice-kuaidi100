'use strict'

require('dotenv').config()

const Glue = require('@hapi/glue')
const manifest = require('./config/manifest')

const options = {
  relativeTo: __dirname

}
const startServer = async function () {
  try {
    const server = await Glue.compose(manifest, options)
    // 啟勳
    await server.start()
    console.log(`${new Date()} Server listening on ${server.info.uri}`)
  } catch (err) {
    console.error(err)
    process.exit(1)
  }
}

process.on('unhandledRejection', (err) => {
  console.log(err)
  process.exit(1)
})

startServer()
